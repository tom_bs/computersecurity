from flask import Flask
from login_system.ServerSide.PasswordManager import PasswordManager
from login_system.ServerSide.SessionManager import SessionManager
from login_system.ServerSide.SystemSetup import initial_setup
from login_system.ServerSide.defs import PASSWORD_FILE_PATH

app = Flask(__name__)

app.config.from_object('login_system.settings')

config = app.config
app.url_map.strict_slashes = False

initial_setup()
password_manager = PasswordManager(PASSWORD_FILE_PATH)
session_manager = SessionManager(password_manager)


import login_system.api
import ServerSide
