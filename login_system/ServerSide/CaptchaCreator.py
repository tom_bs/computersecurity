from captcha.image import ImageCaptcha
import string
import random

WORD_SIZE = 6
NUM_OF_WORDS = 1


class CaptchaCreator(object):
    """
    Creates captcha images
    """

    def __init__(self):
        """Return a Captcha object"""
        self._captcha_maker = ImageCaptcha()

    @staticmethod
    def _create_random_text():
        words = [''.join(random.choice(string.ascii_letters + string.digits)
                         for _ in range(WORD_SIZE)) for _ in range(NUM_OF_WORDS)]
        return " ".join(words)

    def create_captcha(self):
        """Create a new random captcha and uid"""

        captcha_text = self._create_random_text()
        captcha_pic_data = self._captcha_maker.generate(captcha_text).read()
        encoded_captcha_pic = captcha_pic_data.encode("base64")

        return encoded_captcha_pic, captcha_text
