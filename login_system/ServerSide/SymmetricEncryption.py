import os
from Crypto import Random
from Crypto.Cipher import AES
from pkcs7 import PKCS7Encoder
import base64

AES_BLOCK_SIZE = 16


class SymmetricEncryption(object):
    """Use AES mode CBC for session encryption so each block is provided with both authentication and privacy"""

    def __init__(self, symmetric_key=None):
        """Return a SymmetricEncryption object

        :param symmetric_key: the key to use for the encryption.
        Will be generated automatically if wrong or not given
        """
        if not symmetric_key or len(symmetric_key) != AES_BLOCK_SIZE:
            self.session_key = os.urandom(16)
        else:
            self.session_key = symmetric_key
        self.mode = AES.MODE_CBC
        self.encoder = PKCS7Encoder()

    def encrypt(self, text):
        raw = self.encoder.encode(text)
        iv = Random.new().read(AES_BLOCK_SIZE)
        cipher = AES.new(self.session_key, self.mode, iv)
        encoded_message = base64.b64encode(iv + cipher.encrypt(raw))
        return encoded_message

    def decrypt(self, ciphertext):
        decoded_text = base64.b64decode(ciphertext).decode("hex")
        iv = decoded_text[:AES_BLOCK_SIZE]
        aes = AES.new(self.session_key, self.mode, iv)
        padded_text = aes.decrypt(decoded_text[AES_BLOCK_SIZE:])
        text = self.encoder.decode(padded_text)
        return text
