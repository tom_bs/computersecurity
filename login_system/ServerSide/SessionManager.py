import time
import uuid
import os
import threading
from CaptchaCreator import CaptchaCreator
from SymmetricEncryption import AES_BLOCK_SIZE
from defs import enum

# The allowed time for a session to be alive in seconds
ALLOWED_TIME = 60 * 60  # 1 Hour
# The number of times we allow failed login attempts without captcha
MAXIMUM_FAILED_ATTEMPTS_WITHOUT_CAPTCHA = 1
# The number of times we allow failed login attempts PER USER without timeout
MAXIMUM_FAILED_ATTEMPTS_WITHOUT_TIMEOUT = 3
# The time cycle interval in which to check for expired sessions in seconds
TIMEOUT_CHECK_INTERVAL = 60  # 1 Minute
# Time a session remains blocked in seconds
BLOCK_TIMEOUT = 3 * 60  # 3 Minute

RequestStatus = enum(
    'OK',                           # operation succeeded
    'CAPTCHA_MISSING',              # need to request new captcha - get_new_captcha
    'INCORRECT_CAPTCHA',
    'INCORRECT_USER_OR_PASSWORD',
    'BLOCKED',
    'INVALID_SESSION',
    'INVALID_USERNAME',
)


class Session(object):
    """
    Holds information about a session
    """

    def __init__(self):
        """Return a new session object"""

        self.time = time.time()
        self.key = os.urandom(AES_BLOCK_SIZE)
        self.captcha_text = None
        # the number of failed login attempts for each
        self.users_to_failed_attempts = {}
        # block a session for a given time
        self.block_time = None

    def failed_user_login(self, username):
        if username in self.users_to_failed_attempts:
            self.users_to_failed_attempts[username] += 1
        else:
            # the user has failed once
            self.users_to_failed_attempts[username] = 1

    def should_user_be_blocked(self, username):
        return self.users_to_failed_attempts[username] >= MAXIMUM_FAILED_ATTEMPTS_WITHOUT_TIMEOUT


class SessionManager(object):
    """
    Manages sessions: uids and keys at server side.
    responsible for storing and creating session uid and and respective symmetric encryption keys.
    """

    def __init__(self, password_manager, remove_timeout_sessions_automatically=True):
        """Return a PasswordManager object"""
        self._timer = None
        self._password_manager = password_manager
        self._captcha_creator = CaptchaCreator()
        self._uid_to_session = {}
        self._remove_timeout_sessions_automatically = None
        self.remove_timeout_sessions_automatically = remove_timeout_sessions_automatically

    @property
    def remove_timeout_sessions_automatically(self):
        return self._remove_timeout_sessions_automatically

    @remove_timeout_sessions_automatically.setter
    def remove_timeout_sessions_automatically(self, value):
        if self._remove_timeout_sessions_automatically != value:
            self._remove_timeout_sessions_automatically = value
            if self._remove_timeout_sessions_automatically:
                # call clear function will cause it to be called automatically
                self._clear_timeout_sessions()

    def create_new_session(self):
        uid = self._create_uid()
        session = Session()
        self._uid_to_session[uid] = session

        return uid, session

    def get_session(self, uid):
        try:
            return self._uid_to_session[uid]
        except KeyError:
            return None

    def close(self):
        self.remove_timeout_sessions_automatically = False
        self._timer.cancel()

    def signup(self, uid, user_name, hashed_password, captcha_solution):
        session = self.get_session(uid)

        if session is None:
            return RequestStatus.INVALID_SESSION

        if session.block_time is not None and (time.time() - session.block_time) < BLOCK_TIMEOUT:
            session.users_to_failed_attempts = {}
            return RequestStatus.BLOCKED

        if not session.captcha_text:
            return RequestStatus.CAPTCHA_MISSING

        if session.captcha_text.lower() != captcha_solution.lower():
            session.captcha_text = None
            return RequestStatus.INCORRECT_CAPTCHA

        if not self._password_manager.add_entry(user_name, hashed_password):
            return RequestStatus.INVALID_USERNAME

        self._uid_to_session.pop(uid)
        return RequestStatus.OK

    def login(self, uid, user_name, hashed_password, captcha_solution=None):
        session = self.get_session(uid)

        if session is None:
            return RequestStatus.INVALID_SESSION

        if session.block_time is not None and (time.time() - session.block_time) < BLOCK_TIMEOUT:
            session.users_to_failed_attempts = {}
            return RequestStatus.BLOCKED

        captcha_needed = user_name in session.users_to_failed_attempts
        if captcha_needed and not session.captcha_text:
            return RequestStatus.CAPTCHA_MISSING

        if captcha_needed and session.captcha_text.lower() != captcha_solution.lower():
            # in this case the captcha exists and is not correct
            session.failed_user_login(user_name)
            if session.should_user_be_blocked(user_name):
                session.block_time = time.time()
                return RequestStatus.BLOCKED
            return RequestStatus.INCORRECT_CAPTCHA

        if not self._password_manager.check_password(user_name, hashed_password):
            session.failed_user_login(user_name)
            if session.should_user_be_blocked(user_name):
                session.block_time = time.time()
                return RequestStatus.BLOCKED
            return RequestStatus.INCORRECT_USER_OR_PASSWORD

        self._uid_to_session.pop(uid)
        return RequestStatus.OK

    def get_session_key(self, uid):
        session = self.get_session(uid)

        if session is None:
            return None

        return session.key

    def get_new_captcha(self, uid):
        """creates a new captcha and set it in the appropriate session"""
        session = self.get_session(uid)

        if session is None:
            return RequestStatus.INVALID_SESSION

        encoded_captcha_pic, captcha_text = self._captcha_creator.create_captcha()
        session.captcha_text = captcha_text
        print captcha_text
        return encoded_captcha_pic

    def _create_uid(self):
        uid = uuid.uuid4()
        # for the slightest change we have uuid conflict
        while uid in self._uid_to_session:
            uid = uid.uuid4()

        return str(uid)

    def _should_section_removed(self, uid):
        """
        returns true iff the session should not be removed now
        """
        session = self._uid_to_session[uid]

        # if there is timeout and it has not yet elapsed, we don't want to remove the session yet
        # so we can return this state to the user
        if session.block_time is not None and (time.time() - session.block_time) < BLOCK_TIMEOUT:
            return False

        session_duration = time.time() - session.time
        return session_duration > ALLOWED_TIME

    def _clear_timeout_sessions(self):
        """
        Remove sessions whose timeout has expired
        This function is recursively calling itself automatically within a given timeout
        """
        if not self.remove_timeout_sessions_automatically:
            return
        for uid in self._uid_to_session:
            if self._should_section_removed(uid):
                self._uid_to_session.pop(uid)

        # call this function again in TIMEOUT_CHECK_INTERVAL
        self._timer = threading.Timer(TIMEOUT_CHECK_INTERVAL, self._clear_timeout_sessions)
        self._timer.start()
