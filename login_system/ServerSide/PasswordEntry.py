import hmac
import hashlib
from AsymmetricKey import get_key_from_file
from defs import PRIVATE_KEY_FILE_PATH, SECRET_CODE

HMAC_KEY = "1234567890123456"


class PasswordEntry(object):
    """
    Represents a password entry from the password file
    """
    def __init__(self, user_name, salt, password_hash, hmac_value=None):
        """Return a PasswordEntry object"""
        self.user_name = user_name
        self.salt = salt

        # if hmac is not given, we assume it's a new user record, otherwise it might be an existing user
        if hmac_value is not None:
            # the password_hash has already been hashed since it's existing user
            self.hash_value = password_hash
            # Assign the already given hmac value
            self.hmac_value = hmac_value
            if not self._is_valid():
                raise Exception("signature does not match user data")
        else:
            self.hash_value = self.calculate_hash(salt, password_hash)
            self.hmac_value = self._calculate_hmac()

    @staticmethod
    def calculate_hash(salt, hashed_password):
        string_to_hash = salt + hashed_password
        return hashlib.sha256(string_to_hash).hexdigest()

    @classmethod
    def from_string(cls, entry_str):
        """Creates new entry from a string representation"""
        try:
            user_name, salt, hash_value, hmac_value = entry_str.split('\t')
        except ValueError:
            print "Could not parse given Password Entry"
            return None
        return cls(user_name, salt, hash_value, hmac_value)

    def to_string(self, with_hmac=True):
        """Creates string representation of the entry"""
        string_without_hmac = "\t".join((self.user_name, self.salt, self.hash_value))
        if not with_hmac:
            return string_without_hmac
        hmac_value = self._calculate_hmac()
        return "\t".join((string_without_hmac, hmac_value))

    def _is_valid(self):
        """validate hmac of an entry"""
        return self.hmac_value == self._calculate_hmac()

    def _calculate_hmac(self):
        """Calculate hmac for the entry"""
        key = get_key_from_file(PRIVATE_KEY_FILE_PATH, SECRET_CODE)
        #TODO
        return hmac.new(HMAC_KEY, self.to_string(with_hmac=False), hashlib.sha256).hexdigest()
