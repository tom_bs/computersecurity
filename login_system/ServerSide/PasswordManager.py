import random
import string
from PasswordEntry import PasswordEntry
from defs import PASSWORD_FILE_PATH


class PasswordManager(object):
    """
    Manages passwords at server side.
    Responsible for all password-related tasks.
    """

    def __init__(self, password_file_path=PASSWORD_FILE_PATH):
        """Return a PasswordManager object"""
        self._change_flag = False  # true iff there the entries in memory and in the disk are different
        self._file_path = password_file_path
        self._entries = []
        self.read_entries()

    def read_entries(self):
        """Set the password entries from the password file"""
        with open(self._file_path) as password_file:
            entries_lines = password_file.readlines()
        entries_str = [line.strip() for line in entries_lines]
        for entry_str in entries_str:
            self._entries.append(PasswordEntry.from_string(entry_str))

    def write_entries(self):
        """write to the password file according to the entries"""
        if not self._change_flag:
            return

        entries_str = [entry.to_string() for entry in self._entries]

        with open(self._file_path, "w") as password_file:
            password_file.write("\n".join(entries_str))

    def check_password(self, user_name, hashed_password):
        entry = self._find_entry(user_name)
        if entry is None:
            print "username \"{0}\" could not be found in system".format(user_name)
            return False
        return entry.hash_value == entry.calculate_hash(entry.salt, hashed_password)

    def add_entry(self, user_name, hashed_password):
        """
        Adds an entry corresponding to the given user name and hashed_password
        Note: Does not write to the password file, should also use write_entries
        """
        for entry in self._entries:
            if user_name == entry.user_name:
                # user name is already taken
                return False
        salt_chars = string.ascii_letters + string.digits
        # get random salt value
        salt = ''.join((random.choice(salt_chars) for _ in xrange(32)))
        entry = PasswordEntry(user_name, salt, hashed_password)

        self._entries.append(entry)
        self._change_flag = True
        return True

    def remove_entry(self, user_name):
        """
        Remove entry corresponding to the given user name
        Note: Does not write to the password file, should also use write_entries
        """
        entry = self._find_entry(user_name)
        self._entries.remove(entry)
        self._change_flag = True

    def _find_entry(self, user_name):
        for entry in self._entries:
            if entry.user_name == user_name:
                return entry
        return None
