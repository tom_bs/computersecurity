import os.path


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

ASYMMETRIC_KEY_BIT_SIZE = 4096
SECRET_CODE = "THIS_IS_PASSWORD_FOR_PRIVATE_KEY"

FILES_PERMISSION_MASK = 0600  # owner read-write

PASSWORD_FILE_PATH = r".\\passwords.txt"

KEYS_DIR = r".\\keys\\"
PRIVATE_KEY_FILE_NAME = "private.pem"
PRIVATE_KEY_FILE_PATH = os.path.join(KEYS_DIR, PRIVATE_KEY_FILE_NAME)
PUBLIC_KEY_FILE_NAME = "public.pem"
PUBLIC_KEY_FILE_PATH = os.path.join(KEYS_DIR, PUBLIC_KEY_FILE_NAME)
