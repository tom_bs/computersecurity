from Crypto.Cipher import PKCS1_v1_5


class PublicKeyEncryptor(object):
    """ Encryption using Public key """

    def __init__(self, public_key):
        """Return a PublicKeyEncryptor object

        :param public_key: public key for the encryption
        """
        self.public_key = public_key

    def encrypt(self, data):
        """Encrypt data using the RSA public key
        We use RSA with PKCS1_v1_5 Standard for asymmetric encryption """
        if not self.public_key:
            raise Exception("No Public Key")
        cipher_rsa = PKCS1_v1_5.new(self.public_key)
        return cipher_rsa.encrypt(data)


class PrivateKeyEncryptor(object):
    """ Decryption using private key """
    def __init__(self, private_key=None):
        """Return a PrivateKeyEncryptor object

        :param private_key: private key for the encryption
        """
        self.private_key = private_key

    def decrypt(self, data):
        """Decrypt data using the RSA private key
        We use RSA with PKCS1_v1_5 Standard for asymmetric encryption """
        if not self.private_key:
            raise Exception("No Private Key")
        cipher_rsa = PKCS1_v1_5.new(self.private_key)
        senital = []
        result = cipher_rsa.decrypt(data, senital)
        if result == senital:
            print "Error while Decrypting"
            return None
        return result
