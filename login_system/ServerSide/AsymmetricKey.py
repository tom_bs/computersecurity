from Crypto.PublicKey import RSA
from Crypto import Random
from defs import SECRET_CODE, ASYMMETRIC_KEY_BIT_SIZE


def generate_key_pair(public_key_path, private_key_path, secret_code=SECRET_CODE):
    """
    Generates a new RSA key pair in ASCII/PEM format and saves it into a private_key_file and public_key_file.
    the private key is protected by a password.
    We use the scrypt key derivation function to thwart dictionary attacks.
    """
    random_generator = Random.new().read
    key = RSA.generate(ASYMMETRIC_KEY_BIT_SIZE, random_generator)

    # encrypt the private key with scrypt and AES 128 bit CBC mode
    encrypted_private_key = key.exportKey(passphrase=secret_code, pkcs=8, protection="scryptAndAES128-CBC")
    public_key = key.publickey().exportKey()
    with open(private_key_path, "wb") as private_key_file:
        private_key_file.write(encrypted_private_key)
    with open(public_key_path, "wb") as public_key_file:
        public_key_file.write(public_key)


def get_key_from_file(key_path, secret_code=None):
    """
    imports a saved RSA key from ASCII/PEM format in specified file.
    :param key_path: key file path
    :param secret_code: if secret_code is specified then key imported key is private.
    """
    try:
        with open(key_path, "r") as key_file:
            key_data = key_file.read()
    except EnvironmentError:  # parent of IOError, OSError *and* WindowsError where available
        print "Could not found specified key_path: \"{0}\"".format(key_path)
        return None

    return get_key_from_string(key_data, secret_code)


def get_key_from_string(key_string, secret_code=None):
    """
    imports a saved RSA key from ASCII/PEM format.
    :param key_path: key file path
    :param secret_code: if secret_code is specified then key imported key is private.
    """
    key = RSA.importKey(key_string, secret_code)
    return key
