import os
from defs import KEYS_DIR, PRIVATE_KEY_FILE_PATH, PUBLIC_KEY_FILE_PATH, FILES_PERMISSION_MASK
from defs import PASSWORD_FILE_PATH
from AsymmetricKey import generate_key_pair


def is_initial_setup():
    return not os.path.exists(PRIVATE_KEY_FILE_PATH) or not os.path.exists(PRIVATE_KEY_FILE_PATH)


def initialize():
    # create empty passwords file if does not exists
    open(PASSWORD_FILE_PATH, 'a').close()
    os.chmod(PASSWORD_FILE_PATH, FILES_PERMISSION_MASK)

    if not os.path.exists(KEYS_DIR):
        os.mkdir(KEYS_DIR, FILES_PERMISSION_MASK)
    generate_key_pair(PUBLIC_KEY_FILE_PATH, PRIVATE_KEY_FILE_PATH)
    os.chmod(PUBLIC_KEY_FILE_PATH, FILES_PERMISSION_MASK)
    os.chmod(PRIVATE_KEY_FILE_PATH, FILES_PERMISSION_MASK)


def initial_setup():
    if not is_initial_setup():
        return
    else:
        print "Initializing system"
        initialize()
