app.controller('loginCtrl', ['$scope', '$log', '$location', 'authentication','clientKey','toaster', function loginController($scope, $log, $location, authentication,clientKey,toaster) {
    $scope = this;

    $scope.signin_valid_form = false;
    $scope.signup_valid_form = false;
    $scope.newUser = false;
    $scope.user = '';
    $scope.password = '';
    $scope.password_retype = '';
    $scope.captcha ='';
    $scope.captcha_txt ='';
    $scope.signin = 'Login';
    $scope.signup = 'Sign Up';
    $scope.dataLoading = false;
    $scope.invalid_pass_list = [];
    $scope.error_message = '';
    var uid ='';
    var serverPublickey = '';
    var symmetricKey = '';
    $scope.clientPublickey = "";
    $scope.clientPrivatekey = "";

    function getServerPublickKey(){
      authentication.GetServerPublicKey(function(response) {
          if (response.status == 200) {
            serverPublickey = response.data.key;
          } else {
              toaster.pop('error',  'Service is currently unavailable');
          }
      });
    }

    $scope.init = function(){

      $scope.clientPrivatekey =clientKey.getClientPrivatekey();
      $scope.clientPublickey = clientKey.getClientPublickey();
      $scope.dataLoading = true;
      authentication.sendClientPublicKey($scope.clientPublickey,$scope.clientPrivatekey,function(response) {
        if(response){
          switch(response.status) {
            case 200:
              symmetricKey = response.message.key;
              uid = response.message.uid;
              $scope.dataLoading = false;
              break;
            case 406:
              toaster.pop('error',  'Failed to send client public key');
              break;
            default:
              toaster.pop('error',  'Service is currently unavailable');
          }
        }
        else{
            toaster.pop('error',  'Service is currently unavailable');
        }
      });
      authentication.CommonPasswordList(function(response) {
          if (response.status == 200) {
            $scope.invalid_pass_list = response.data.invalid.slice();
          }
        });
    }

    $scope.Redirect = function(path) {
        $log.debug('redirecting.. ' + path);
        $location.path(path);
    }
    $scope.signInValidation = function () {

      if(!$scope.user || !$scope.password || ($scope.captcha && !$scope.captcha_txt)){
        $scope.signin_valid_form = false;
      } else{
        $scope.signin_valid_form = true;
      }
      return $scope.signin_valid_form;

    }
    $scope.signUpValidation = function () {
      if(!$scope.user || !$scope.password ||  !$scope.password_retype || !$scope.captcha || !$scope.captcha_txt ){
        $scope.signup_valid_form = false;
      } else{
        $scope.signup_valid_form = true;
      }
      return $scope.signup_valid_form
    }
    $scope.clearErrorMessage=function(){
      $scope.error_message ='';
    }

    $scope.signIn = function(){
      if($scope.newUser){
          $scope.newUser = false;
          var login = $scope.signin;
          var signup = $scope.signup;
          clearFields('Login', 'Sign Up', false);
      } else{
        var pass_hash = calculateHash($scope.password);
        $log.info("signIn request | sent to server ");
        var captchaRequired = false;
        if($scope.captcha != ''){
          captchaRequired = true;
        }
        authentication.signIn(uid, $scope.user ,pass_hash,$scope.captcha_txt,symmetricKey,function( response) {
          if(response){
            switch(response.status) {
              case 200:
                $log.info("user successfully logged in");
                alert("Welcome back " + $scope.user + "\nhave a good day");
                clearFields('Login', 'Sign Up');
                $scope.newUser = false;
                $scope.Redirect('/');
                break;
              case 203:
                $scope.dataLoading = true;
                authentication.sendClientPublicKey($scope.clientPublickey,$scope.clientPrivatekey,function(response) {
                  var status = startSessionAction(response);
                  if (status == 200 ){
                    if(captchaRequired){
                      toaster.pop('error',  "Please answer the new  captcha challenge");
                      $scope.GetCaptcha();
                    } else{
                      $scope.signIn();
                    }
                  }
                });
                break;
              case 300:
                captchaRequired = true;
                $scope.error_message= 'captcha challenge must be answered due to failed entry attempt';
                $scope.captcha_txt = '';
                $scope.GetCaptcha();
                break;
              case 403:
                $scope.error_message= 'incorrect username or password';
                captchaRequired = true;
                $scope.captcha_txt = '';
                $scope.user = '';
                $scope.password = '';
                $scope.GetCaptcha();
                break;
              case 406:   // captcha is not correct
                toaster.pop('error',  "Invalid captcha text value");
                clearFields('Login', 'Sign Up');
                $scope.GetCaptcha();
                break;
              case 408:   // timeout
                toaster.pop('error',  "blocked until timeout");
                clearFields('Login', 'Sign Up');
                break;
              default:
                toaster.pop('error',  response.message);
                clearFields('Login', 'Sign Up');
                if(captchaRequired){
                    $scope.GetCaptcha();
                }
            }
          }
          else{
              toaster.pop('error',  'Service is currently unavailable');
          }
      });
    }
  }

    $scope.signUp = function(){
      if(!$scope.newUser){
          $scope.newUser = true;
          clearFields('Go Back', 'Create Account', false);
          $log.info("get captch from server");
          $scope.GetCaptcha();
      } else{

        if ($scope.password != $scope.password_retype){
          $scope.error_message= 'passwords are not the same';
        }
        else{
          var pass_is_strong = isPasswordStrong($scope.password);
          var from_leet = leet_decode($scope.password);
          var pass_is_common = passIsCommon($scope.password) || passIsCommon(from_leet);

          if (pass_is_strong  && !pass_is_common ){
            var pass_hash = calculateHash($scope.password);
            $log.info("signUp request | sent to server ");

            authentication.signUp(uid, $scope.user ,pass_hash,$scope.captcha_txt,symmetricKey,function( response) {
              if(response){
                switch(response.status) {
                  case 201:
                    $log.info("user successfully created");
                    alert("Welcome " + $scope.user + "\nhave a good day");
                    clearFields('Login', 'Sign Up');
                    $scope.newUser = false;
                    $scope.Redirect('/');
                    break;
                  case 203:
                    toaster.pop('error',  "Please answer the new  captcha challenge");
                    $scope.dataLoading = true;
                    authentication.sendClientPublicKey($scope.clientPublickey,$scope.clientPrivatekey,function(response) {
                    var status = startSessionAction(response);
                    if (status == 200){
                      $scope.GetCaptcha();
                    }
                  });
                    break;
                  case 403:  // username exists
                    $scope.error_message= 'username already exists';
                    $scope.user = '';
                    $scope.captcha_txt = '';
                    $scope.GetCaptcha();
                    break;
                  case 406:   // captcha is not correct
                    toaster.pop('error',  "Invalid captcha text value");
                    clearFields('Go Back', 'Create Account');
                    $scope.GetCaptcha();
                    break;
                  case 408:   // timeout
                    toaster.pop('error',  "blocked until timeout");
                    $scope.newUser = false;
                    clearFields('Login', 'Sign Up');
                    break;
                  default:
                    toaster.pop('error',  response.message);
                    clearFields('Go Back', 'Create Account');
                    $scope.GetCaptcha();
                }
              }
              else{
                  toaster.pop('error',  'Service is currently unavailable');
              }
          });
        }
        else{
          console.log($scope.error_message);
          $scope.error_message= 'Password is weak, please enter a new password';
        }

        }
      }
    }

    function clearFields(login,signup, cleaar_all = true){

      $scope.signin = login;
      $scope.signup = signup;
      if(cleaar_all){
        $scope.user = '';
        $scope.password = '';
      }
      $scope.password_retype = '';
      $scope.captcha_txt = '';
      $scope.captcha = '';
      $scope.error_message='';

    }

    function calculateHash(s){
      return md5(s);
    }

    function isPasswordStrong(s){
      var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
      return strongRegex.test(s);
    }

    function passIsCommon(s){
      return $scope.invalid_pass_list.indexOf(s) > -1;
    }
    
    $scope.GetCaptcha = function() {
        $scope.dataLoading = true;
        authentication.GetCaptcha(uid, symmetricKey, function(response) {
          if(response){
            switch( response.status) {
              case 200:
                $log.info("get Captcha Succeeded")
                $scope.captcha = response.message;
                $scope.dataLoading = false;
                break;
              case 203:
                authentication.sendClientPublicKey($scope.clientPublickey,$scope.clientPrivatekey,function(response) {
                  var status = startSessionAction(response);
                  if (status == 200){
                    $scope.GetCaptcha();
                  }
                });
                break;
              case 101:
                $log.info("get Captcha failed | " + response.message + " status 101" )
                toaster.pop('error', response.message);
                break;
              default:
                toaster.pop('error',  'Service is currently unavailable');
            }
          }
          else{
            $log.info("get Captcha failed")
            toaster.pop('error',  'Service is currently unavailable');
          }
        });
      }
    function startSessionAction(response){
      if(response){
        switch(response.status) {
          case 200:
            symmetricKey = response.message.key;
            uid = response.message.uid;
            $scope.dataLoading = false;
            break;
          case 406:
            toaster.pop('error',  'Failed to send client public key');
            break;
          default:
            toaster.pop('error',  'Service is currently unavailable');
        }
        return response.status;
      }
      else{
          toaster.pop('error',  'Service is currently unavailable');
        return 0;
      }
    }
    function array_flip( trans ){
      var key, tmp_ar = {};
      for ( key in trans ){
          if ( trans.hasOwnProperty( key ) ){
              tmp_ar["[" + trans[key] + "]"] = key;
          }
      }
      return tmp_ar;
    }
    

   var english = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q","r","s","t","u","v","w","x","y","z"];
   var leet = [["4" , "@" ,"/-\\"	],
   ["8", "|3"],
   ["("],
   ["|)"],
   ["3"],
   ["|=","pH"],
   ["9", "6"],
   ["|-|", "#"],
   ["1", "|", "!"],
   ["j",";"],
   ["|{", "|<"],
   ["|_","[]_", "|"],
   ["|\\/|", ")v("],
   ["|\\|" , "/\\/"],
   ["0","()"],
   ["p","|>"],
   ["q"],
   ["|2"],
   ["5", "$"],
   ["+", "7"],
   ["|_| ", "\\_/"],
   ["\\/"],
   ["\\/\\/",  "\\X/", " \\^/"],
   ["x", "X" , "><"],
   ["y" ,"'/"],
   ["z", "2"]];

   flippedLeetDict = array_flip(leet);
   function leet_decode(leet_str) {
     leet_str = leet_str.toLowerCase();
     for(var i = 0; i<leet_str.length;i++ )
     {
       len = Math.abs(leet_str.length - i);
       trans_char = leetToStr(leet_str.substring(i, i + len),len)
       leet_str = leet_str.substring(0,i)+ trans_char + leet_str.substring(i + len, leet_str.length);
     } 
     return leet_str;
   }

   function leetToStr(str,len){
     var trans_char =''
     for(var index  in  flippedLeetDict){
      var indexlen = index.length;
      var index_arr = index.substring(1, indexlen -1).split(",");
      if(index_arr.indexOf(str) > -1){
         trans_char = english[flippedLeetDict[index]]
         break;
       }
     }
     if(len > 1 && trans_char == ''){
       return leetToStr(str.substring(0, len -1), len -1) + str.substring(len -1, len)
     }
     return trans_char? trans_char: str;
   }
   

}]);
