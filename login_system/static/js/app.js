'use strict';

var app = angular.module('LoginApp', ['authenticationService','clientKeyService']);
app.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
  $routeProvider
		.when('/', {
			templateUrl: '../static/partials/login.html',
			controller: 'loginCtrl',
      controllerAs: 'login'
		})
    .when('/key', {
			templateUrl: '../static/partials/clientKey.html',
			controller: 'keyCtrl',
      controllerAs: 'key'
		})
		.otherwise({
			templateUrl: '../static/partials/404.html'
		});
	}])
