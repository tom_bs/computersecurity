app.controller('keyCtrl', ['$scope', '$log', '$location', 'clientKey','toaster', function loginController($scope, $log, $location,clientKey,toaster) {
    $scope = this;

    $scope.clientPublickey = "";
    $scope.clientPrivatekey = "";

    $scope.init = function(){
      $scope.clientPrivatekey =clientKey.getClientPrivatekey();
      $scope.clientPublickey = clientKey.getClientPublickey();

    }
    $scope.setKey = function(){
      $log.info('setKey function');
      clientKey.setClientPrivatekey($scope.clientPrivatekey);
      clientKey.setClientPublickey($scope.clientPublickey);
    }

    $scope.Redirect = function(path) {
      $log.debug('redirecting.. ' + path);
      $location.path(path);
    }

}]);
