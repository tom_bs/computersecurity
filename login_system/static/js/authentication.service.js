'use strict';
angular.module('authenticationService', ['toaster','ngRoute'])
    .factory('authentication', ['$http', '$log', function($http, $log) {
        var service = {};
        function Respond (status, message='') {
            this.status = status;
            this.message = message;
        }

        function getRandomIv() {
            var rnd =  Math.random() * (9999999999999999 - 1000000000000000) + 1000000000000000;
            return parseInt(rnd).toString();
        }
        function RSAEncrypt(plaintxt, key) {
          var encrypt = new JSEncrypt();
          encrypt.setPublicKey(key);
          var encrypted = encrypt.encrypt(plaintxt);
          return encrypted;
        }
        function RSADecrypt(encryptedtxt, key) {
          $log.info("RSADecrypt function");
          var decrypt = new JSEncrypt();
          decrypt.setPrivateKey(key)
          var uncrypted = decrypt.decrypt(encryptedtxt);
          return uncrypted;
        }
        function AESEncrypt(plaintxt, key) {
          var rndIv = getRandomIv();
          var iv = CryptoJS.enc.Latin1.parse(rndIv);
          var key = CryptoJS.enc.Latin1.parse(key);
          var encrypted = CryptoJS.AES.encrypt(plaintxt, key, { iv: iv, mode: CryptoJS.mode.CBC  ,padding: CryptoJS.pad.Pkcs7});
          encrypted = encrypted.toString();
          encrypted = iv+encrypted;
          encrypted = btoa(encrypted);
          return encrypted;
        }
        function AESDecrypt(base64ciphertext, key) {
          var rawData = atob(base64ciphertext);
          // Split by 16 because my IV size
          var iv = rawData.substring(0, 16);
          var crypttext = rawData.substring(16);
          //Parsers
          crypttext = CryptoJS.enc.Latin1.parse(crypttext);
          iv = CryptoJS.enc.Latin1.parse(iv);
          var key = CryptoJS.enc.Latin1.parse(key);
          // Decrypt
          var plaintextArray = CryptoJS.AES.decrypt(
            { ciphertext:  crypttext},
            key,
            {iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7}
          );
          // Can be Utf8 too
          var output_plaintext = CryptoJS.enc.Latin1.stringify(plaintextArray);
          return output_plaintext;
        }

        service.GetServerPublicKey = function(callback){
          $http.get('/api/get_public_key').
          then(function successCallback(response) {
              $log.info("GetServerPublicKey function | successCallback");
              callback(response);
          }, function errorCallback(response) {
              $log.info("GetServerPublicKey function | errorCallback");
              callback(response);
          });

        }

        service.sendClientPublicKey = function(client_public_key,client_private_key,callback) {
          var message = {key:client_public_key};
          $http.post('/api/send_client_public_key', message).
            then(function successCallback(response) {
                $log.info("Successfully received | send_client_public_key");
                var uncrypted = RSADecrypt(response.data.message, client_private_key);
                var uncrypted = JSON.parse(uncrypted);

                if (uncrypted == null) {
                  $log.info("decrypt failed | send_client_public_key");
                  callback(new Respond(406, ""))
                }else{
                  var message = {"uid":uncrypted.uid, "key":atob(uncrypted.key)}
                  callback(new Respond(200,message ));
                }

            }, function errorCallback(response) {
                $log.info("Error received | send_client_public_key");
                callback(response);
            });
        }

        service.GetCaptcha = function(uid, symmetricKey, callback) {
            $http.post('/api/get_captcha',{uid:uid}).
            then(function successCallback(response) {
                $log.info("getCaptcha function | successCallback");
                if ( response.status == 203 ){
                  callback(new Respond(203, response.data.message))
                }
                else{
                  var uncrypted = AESDecrypt(response.data.message, symmetricKey);

                  if (uncrypted == null) {
                    $log.info("decrypt failed | send_client_public_key");
                    callback(new Respond(406, ""))
                  }else{
                    try {
                      var uncrypted = JSON.parse(uncrypted);
                      if (uncrypted.uid != uid) {
                          callback(new Respond(409 ,"Authentication failed conflict with key"));
                      }
                      else {
                        var captcha = uncrypted.encoded_captcha;
                        callback(new Respond(200,captcha));
                      }
                    } catch(e) {
                      $log.info(e)
                      service.GetCaptcha(uid,symmetricKey,callback);
                    }
                  }

                }
                
             }, function errorCallback(response) {
                 $log.info("getCaptcha function | errorCallback");
                 callback(response);
             });
        };

        service.CommonPasswordList = function(callback) {
          $http.get('/api/get_common_passwords').
          then(function successCallback(response) {
              $log.info("getCommonPass function | successCallback");
              callback(response);
          }, function errorCallback(response) {
              $log.info("getCommonPass function | errorCallback");
              callback(response);
          });
        };

        service.signIn = function(uid, username,pass_hash,captcha_txt,symmetricKey,callback){
          var message = {"uid":uid,
                        "username":username,
                        "hash_password": pass_hash,
                        "captcha_sultion": captcha_txt};
          var ciphertext = AESEncrypt(JSON.stringify(message),symmetricKey) ;

          var json_message = { "uid":uid,
            "message":ciphertext
          }
          $http.post('/api/login', json_message).
            then(function successCallback(response) {
                $log.info("Successfully received | signIn");
                if ( response.status == 203 ){
                  callback(new Respond(203, response.data.message))
                }
                else {
                  var uncrypted = AESDecrypt(response.data.message, symmetricKey);
                  if (uncrypted == null) {
                    $log.info("decrypt failed | send_client_public_key");
                    callback(new Respond(406, ""))
                  }else{
                    var uncrypted = JSON.parse(uncrypted);
                    if (uncrypted.uid != uid) {
                        callback(new Respond(409 ,"Authentication failed conflict with key"));
                    }
                    else {
                      callback(new Respond(uncrypted.status, uncrypted.message));
                    }
                  }

                }

            }, function errorCallback(response) {
                $log.info("Error received | signup");
                callback(new Respond(response.status, response.data.message));
            });

        }

        service.signUp = function(uid, username,pass_hash,captcha_txt,symmetricKey,callback){
          var message = {"uid":uid,
                        "username":username,
                        "hash_password": pass_hash,
                        "captcha_sultion": captcha_txt};
          var ciphertext = AESEncrypt(JSON.stringify(message),symmetricKey) ;

          var json_message = { "uid":uid,
            "message":ciphertext
          }
          $http.post('/api/signup', json_message).
            then(function successCallback(response) {
                $log.info("Successfully received | signup");

                if ( response.status == 203 ){
                  callback(new Respond(203, response.data.message))
                }
                else {
                  var uncrypted = AESDecrypt(response.data.message, symmetricKey);
                  if (uncrypted == null) {
                    $log.info("decrypt failed | send_client_public_key");
                    callback(new Respond(406, ""))
                  }else{
                    var uncrypted = JSON.parse(uncrypted);
                    if (uncrypted.uid != uid) {
                        callback(new Respond(409 ,"Authentication failed conflict with key"));
                    }
                    else {
                      callback(new Respond(uncrypted.status, uncrypted.message));
                    }
                  }

                }

            }, function errorCallback(response) {
                $log.info("Error received | signup");
                callback(new Respond(response.status, response.data.message));
            });

        }


        return service;
    }]);
