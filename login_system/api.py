from flask import Response, jsonify
from flask import send_file
from flask import request
import json
from login_system import app
from login_system.ServerSide.AsymmerticEncryption import PublicKeyEncryptor
from login_system.ServerSide.SymmetricEncryption import SymmetricEncryption
from login_system.ServerSide.AsymmetricKey import get_key_from_string, get_key_from_file
from login_system.ServerSide.defs import PUBLIC_KEY_FILE_PATH
from login_system.ServerSide.SessionManager import RequestStatus
from login_system import session_manager


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route("/")
def index():
    return send_file("templates/index.html")


@app.route("/api/get_common_passwords",  methods=['GET'])
def get_common_passwords():
    lines = [line.rstrip('\n') for line in open(app.config['COMMON_PASS_PATH'])]
    return jsonify(invalid=lines)


@app.route("/api/get_public_key",  methods=['GET'])
def get_public_key():
    key = get_key_from_file(PUBLIC_KEY_FILE_PATH)
    return jsonify(key=key)


@app.route("/api/send_client_public_key", methods=["POST"])
def send_client_public_key():
    uid, session = session_manager.create_new_session()
    client_public_key_string = json.loads(request.data)['key']

    message = {'uid': uid, 'key': session.key.encode("base64")}
    client_public_key = get_key_from_string(client_public_key_string)
    ppe = PublicKeyEncryptor(client_public_key)
    msg = ppe.encrypt(json.dumps(message)).encode("base64")
    return jsonify(message=msg)


@app.route("/api/get_captcha",  methods=['POST'])
def get_captcha():
    data = json.loads(request.data)
    user_uid = data['uid']

    key = session_manager.get_session_key(user_uid)
    result = session_manager.get_new_captcha(user_uid)
    if key is None or result == RequestStatus.INVALID_SESSION:
        raise InvalidUsage('connection terminated ', status_code=203)

    message = {"uid": user_uid, "encoded_captcha": result}
    encryptor = SymmetricEncryption(key)
    encrypted_message = encryptor.encrypt(json.dumps(message))

    return jsonify({"uid": user_uid, "message": encrypted_message})


@app.route("/api/login", methods=["POST"])
def login():
    uid = json.loads(request.data)['uid']
    message = json.loads(request.data)['message']

    key = session_manager.get_session_key(uid)
    if key is None:
        raise InvalidUsage('connection terminated ', status_code=203)

    encryptor = SymmetricEncryption(key)
    decrypted_data = json.loads(encryptor.decrypt(message))

    uid_from_message = decrypted_data['uid']
    if uid != uid_from_message:
        raise InvalidUsage('Authentication failed conflict with key', status_code=409)

    username = decrypted_data['username']
    hash_password = decrypted_data['hash_password']
    captcha = decrypted_data['captcha_sultion']

    #import pdb; pdb.set_trace()
    result = session_manager.login(uid, username, hash_password, captcha)
    if result == RequestStatus.INVALID_SESSION:
        raise InvalidUsage('connection terminated ', status_code=203)
    elif result == RequestStatus.BLOCKED:
        message = {'status': 408, 'message': 'blocked until timeout', 'uid': uid}
    elif result == RequestStatus.CAPTCHA_MISSING:
        message = {'status': 300, 'message': 'captcha solution is missing', 'uid': uid}
    elif result == RequestStatus.INCORRECT_CAPTCHA:
        message = {'status': 406, 'message': 'Invalid captcha text value', 'uid': uid}
    elif result == RequestStatus.INCORRECT_USER_OR_PASSWORD:
        message = {'status': 403, 'message': 'incorrect username or password', 'uid': uid}
    elif result == RequestStatus.OK:
        message = {'status': 200, 'message': 'Welcome back', 'uid': uid}
    encrypted_message = encryptor.encrypt(json.dumps(message))

    return jsonify(uid=uid, message=encrypted_message)


@app.route("/api/signup", methods=["POST"])
def signup():
    uid = json.loads(request.data)['uid']
    key = session_manager.get_session_key(uid)
    if key is None:
        raise InvalidUsage('connection terminated ', status_code=203)
    encryptor = SymmetricEncryption(key)

    # check if uid exists, if not send  raise InvalidUsage('connection terminated ', status_code=203)
    message = json.loads(request.data)['message']
    decrypted_message = json.loads(encryptor.decrypt(message))
    uid_from_message = decrypted_message['uid']
    if uid != uid_from_message:
        raise InvalidUsage('Authentication failed conflict with key', status_code=409)

    username = decrypted_message['username']
    # if username  exists : {'status': 403, 'message': 'username already exists' , 'uid': uid }
    captcha_ans = decrypted_message['captcha_sultion']
    # if capcha is not correct  :  message = {'status': 406, 'message': 'Invalid captcha text value' , 'uid': uid }
    hash_password = decrypted_message['hash_password']
    result = session_manager.signup(uid, username, hash_password, captcha_ans)
    if result == RequestStatus.INVALID_SESSION:
        raise InvalidUsage('connection terminated ', status_code=203)
    elif result == RequestStatus.BLOCKED:
        message = {'status': 408, 'message': 'blocked until timeout', 'uid': uid}
    elif result == RequestStatus.CAPTCHA_MISSING:
        message = {'status': 300, 'message': 'captcha solution is missing', 'uid': uid}
    elif result == RequestStatus.INCORRECT_CAPTCHA:
        message = {'status': 406, 'message': 'Invalid captcha text value', 'uid': uid}
    elif result == RequestStatus.INVALID_USERNAME:
        message = {'status': 403, 'message': 'username already exists', 'uid': uid}
    elif result == RequestStatus.OK:
        message = {'status': 201, 'message': 'Welcome', 'uid': uid}
    encrypted_message = encryptor.encrypt(json.dumps(message))

    return jsonify(uid=uid, message=encrypted_message)
