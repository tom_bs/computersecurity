import os
import getpass
import sys
from login_system import app
from login_system.settings import OS_USER_NAME
from login_system import password_manager
from login_system import session_manager


def runserver():
    port = int(os.environ.get('PORT', 5001))
    # if getpass.getuser() != OS_USER_NAME:
    #     import sys
    #     print "please run this server as follows:"
    #     print "python -u {username} python {script}".format(username=OS_USER_NAME, script=sys.argv[0])
    #     return

    app.run(debug=False, host='0.0.0.0', port=port)


def teardown():
    session_manager.close()
    password_manager.write_entries()


if __name__ == '__main__':
    try:
        print "Running server!"
        runserver()
    finally:
        print "Teardown!"
        teardown()

    sys.exit(0)
